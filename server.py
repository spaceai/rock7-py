from datetime import datetime, timedelta

from flask import Flask, render_template
from flask_restful import Api, Resource, reqparse

import json

from os import listdir


app = Flask(__name__)

#   API
#
api = Api(app)

ts = datetime.fromtimestamp(datetime.now().timestamp())

class Log(Resource):
    def get(self):
        return entries, 200
        # return { "log": entries, "dt": (ts - timedelta(hours=-1)).strftime('%Y-%m-%d %H:%M') }, 200

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('device_type')
        parser.add_argument('serial')
        parser.add_argument('momsn')
        parser.add_argument('transmit_time')
        parser.add_argument('imei')
        parser.add_argument('iridium_latitude')
        parser.add_argument('iridium_longitude')
        parser.add_argument('iridium_cep')
        parser.add_argument('iridium_session_status')
        parser.add_argument('data')
        args = parser.parse_args()

        args['decoded'] = bytearray.fromhex(args['data']).decode(encoding="Latin1")

        ts = datetime.fromtimestamp(datetime.now().timestamp())
        dt = (ts - timedelta(hours=-1)).strftime('%Y-%m-%d %H:%M:%S')
        args['ts'] = dt

        entry = dict(args)
        entries.append(entry)

        fh = open('log/'+ (ts - timedelta(hours=-1)).strftime('%Y%m%d_%H%M%S') +'_'+ str(args['serial']) +'.json', 'w')
        fh.write(json.dumps(entry))
        fh.close()

        return entry, 200   # NOT 201 !!!

api.add_resource(Log, "/log")

#   We don't need no db for this demo
#
entries = []
for file in listdir('log/'):
    if file.endswith('.json'):
        fh = open('log/'+ file, 'r')
        data = json.loads(fh.read())
        fh.close()
        entries.append(data)


#   Home page (Vue)
#
@app.route('/')
def index():
    return render_template('index.html')


app.run(debug=True, host='0.0.0.0', port=5000)
